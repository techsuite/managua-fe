import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { HttpClientModule } from '@angular/common/http';
import { InfoPageComponent } from './pages/info/info.page.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { CenterLayoutComponent } from './layouts/center/center.layout.component';
import { NavBarElementComponent } from './components/nav-bar/nav-bar-element/nav-bar-element.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { WelcomePageComponent } from './pages/welcome/welcome.page.component';
import { ControlPanelPageComponent } from "./pages/control-panel/control-panel.page.component";
import { SearchPageComponent } from "./pages/search/search.page.component";
import { LeftLayoutComponent } from './layouts/left/left.layout.component';
import { FormularioPageComponent } from './pages/formulario/formulario.page.component';
import { ControlPanelPagePCRComponent } from './pages/control-panel-pcr/control-panel-pcr.page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule} from '@angular/material/dialog';
import { AccesoComponent } from './pages/acceso/acceso.component';



@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    InfoPageComponent,
    HomePageComponent,
    CenterLayoutComponent,
    NavBarElementComponent,
    UsuarioComponent,
    WelcomePageComponent,
    ControlPanelPageComponent,
    ControlPanelPagePCRComponent,
    SearchPageComponent,
    FormularioPageComponent,
    LeftLayoutComponent,
    AccesoComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, ReactiveFormsModule, BrowserAnimationsModule
  , MatDialogModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
