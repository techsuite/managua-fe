import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acceso',
  template: '<router-outlet></router-outlet>',
})
export class AccesoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
