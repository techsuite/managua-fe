import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/model/usuario';
import { UsuarioService } from 'src/app/service/usuario.service';
import { LoginService } from 'src/app/service/login.service';
//import { ComprobacionDAO } from 'src/main/app/dao/ComprobacionDAO';

export class DatosIdentifiacion {
  user: string;
  password: string;
}

@Component({
  templateUrl: './home.page.component.html',
  styleUrls: ['./home.page.component.scss'],
})
export class HomePageComponent {
  datos: DatosIdentifiacion = new DatosIdentifiacion();

  constructor(private usuarioService: UsuarioService, private router: Router, public loginService: LoginService) {}

  /*login () {
    let valor : number;
    valor = ComprobacionDAO.comprobacionDAO(this.datos.user, this.datos.password);

  }*/

  login() {
    this.usuarioService
      .getUsuario(this.datos.user, this.datos.password)
      .subscribe({
        next: (respuesta: Usuario) => {
          this.loginService.setAuthId(Usuario.name); //Para seguridad
          sessionStorage.setItem("userActual",this.datos.user);
          sessionStorage.setItem("usuario",respuesta.nombreDoctor);
          this.router.navigate(['/acceso/welcome']);
        },
        error: () => {
          document.getElementById('error').setAttribute('open', 'open');
        },
      });
  }
}
