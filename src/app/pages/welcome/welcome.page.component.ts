import { Component, OnInit } from '@angular/core';
import { ServerStatusService } from 'src/app/service/server-status.service';
import { Router } from '@angular/router';
import { PollingService } from 'src/app/service/polling.service';
import { Prueba } from 'src/app/model/prueba/prueba';

@Component({
  templateUrl: './welcome.page.component.html',
  styleUrls: ['./welcome.page.component.scss'],
})
export class WelcomePageComponent implements OnInit {
  usuario: string;
  serverStatus: string;
  pruebasEjecucionPCR: number;
  pruebasEsperaPCR: number;
  pruebasEjecucionSangre: number;
  pruebasEsperaSangre: number;

  constructor(
    private serverStatusService: ServerStatusService,
    private router: Router,
    private pollingService: PollingService
  ) {
    this.usuario=sessionStorage.getItem("usuario");
  }

  ngOnInit() {
    this.pollingService.pollingEjecucion('Prueba_pcr')
      .subscribe((respuesta: Prueba[]) => this.pruebasEjecucionPCR = respuesta.length);

    this.pollingService.pollingEspera('Prueba_pcr')
      .subscribe((respuesta: Prueba[]) => this.pruebasEsperaPCR = respuesta.length);

    this.pollingService.pollingEjecucion('Prueba_sangre')
      .subscribe((respuesta: Prueba[]) => this.pruebasEjecucionSangre = respuesta.length);

    this.pollingService.pollingEspera('Prueba_sangre')
      .subscribe((respuesta: Prueba[]) => this.pruebasEsperaSangre = respuesta.length);
  }

  irAnalisisSangre(){
    this.router.navigate(['/acceso/ctrlPanel'])
  }

  irPruebasPCR(){
    this.router.navigate(['/acceso/ctrlPanelPCR'])
  }

  logoff(){
    
    sessionStorage.clear();

    this.router.navigate(['/']);
  }

}
