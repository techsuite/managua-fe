import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente/paciente';
import { FormBuilder, Validators } from "@angular/forms";
import { PacienteService } from "src/app/service/paciente.service";
import { Prueba } from 'src/app/model/prueba/prueba';
import { PruebaService } from 'src/app/service/prueba.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';


@Component({
    templateUrl: './formulario.page.component.html',
    styleUrls: ['./formulario.page.component.scss']
})



export class FormularioPageComponent{
    paciente: Paciente;
    prueba: Prueba = new Prueba();
    pacienteAso: Paciente[];
    newPacienteAso: Paciente[] = new Array(1);

    constructor(private form: FormBuilder, private router: Router, private pacienteService: PacienteService, private pruebaService: PruebaService){

    }

    pacienteForm = this.form.group({
        dni: ['', Validators.required],
        nombre: ['', Validators.required],
        apellidos: ['', Validators.required],
        genero: ['', Validators.required],
        nacimiento: ['', Validators.required],
        correo: ['', Validators.required],
      });

      submit() {
        this.paciente = new Paciente(this.pacienteForm.get('dni')?.value,
        this.pacienteForm.get('nombre')?.value,
        this.pacienteForm.get('apellidos')?.value,
        this.pacienteForm.get('correo')?.value,
        this.pacienteForm.get('nacimiento')?.value,
        this.pacienteForm.get('genero')?.value);

        this.pacienteService.newPaciente(this.paciente).subscribe();
        
        this.router.navigate(["/acceso/search"]);
  
      }
      logoff(){
    
        sessionStorage.clear();
    
        this.router.navigate(['/']);
      }
      volver(){
        this.router.navigate(["/acceso/search"]);
      }

      crearPrueba(){
        Swal.fire({
          title: '¿Estás seguro de empezar esta prueba?',
          icon: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sí, adelante',
          cancelButtonText: 'No'
        }).then((result) => {
          if (result.value) {
             
            Swal.fire(
              'Hecho',
              'Prueba comenzando',
              'success'
            );
            this.prueba.dniPaciente=this.pacienteForm.get('dni')?.value;
            this.prueba.dniDoctor=sessionStorage.getItem("userActual");
            if(sessionStorage.getItem("nuevaPrueba")=="pcr"){          
              this.pruebaService.crearPruebaPCR(this.prueba).subscribe();
            }
            if(sessionStorage.getItem("nuevaPrueba")=="sangre"){
              this.pruebaService.crearPruebaSangre(this.prueba).subscribe();
            }

            this.router.navigate(["/acceso/welcome"]);

          } else if (result.dismiss === Swal.DismissReason.cancel) {
            Swal.fire(
              'Cancelado',
              'La prueba no ha comenzado',
              'error'
            )
          }
        })
      }
  
}

