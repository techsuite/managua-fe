import { Component, OnInit } from '@angular/core';
import { ServerStatusService } from 'src/app/service/server-status.service';

@Component({
  templateUrl: './info.page.component.html',
  styleUrls: ['./info.page.component.scss'],
})
export class InfoPageComponent implements OnInit {
  serverStatus: string;

  constructor(private serverStatusService: ServerStatusService) {}

  ngOnInit() {
    this.serverStatusService.getStatus().subscribe((statusResponse) => {
      this.serverStatus = statusResponse.status;
    });
  }
}
