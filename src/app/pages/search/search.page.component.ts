
import { ThrowStmt } from '@angular/compiler';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Paciente } from 'src/app/model/paciente/paciente';
import { PacienteMetadata } from 'src/app/model/paciente/pacienteMedata';
import { Prueba } from 'src/app/model/prueba/prueba';
import { PacienteService } from 'src/app/service/paciente.service';
import { PruebaService } from 'src/app/service/prueba.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';


export class DatosIdentificacion {
  dni: string;
  nombre: string;
  apellidos: string;
}

@Component({
  templateUrl: './search.page.component.html',
  styleUrls: ['./search.page.component.scss'],
})
export class SearchPageComponent {
  hayPacientes: Boolean;
  datos: DatosIdentificacion = new DatosIdentificacion();
  pacienteAso: Paciente[];
  newPacienteAso: Paciente[] = new Array(1);
  prueba: Prueba = new Prueba();
  tipoPrueba: string = sessionStorage.getItem("nuevaPrueba")

  constructor(private pacienteService: PacienteService, private router: Router,
  private pruebaService: PruebaService) {}

  /*login () {
    let valor : number;
    valor = ComprobacionDAO.comprobacionDAO(this.datos.user, this.datos.password);
    
  }*/
  ngOnInit(): void {
    this.pacienteService.setPacs([]);
    
  }
  pacienteDisponibles(){
    if (this.pacienteService.getPacs() !== null) {
      this.pacienteAso = this.pacienteService.getPacs();
    }
    return this.pacienteAso !== null;
  }

  
  /*recargarPag(){
    this.pacienteService.getPacientes(this.pacienteService.getPageNum(),this.pacienteService.getPageSize())
    .subscribe((resp: PacienteMetadata)=>{
      this.pacienteAso=resp;
      this.pacienteService.setPacs(resp);
      sessionStorage.setItem('pacientesMostrados',JSON.stringify(this.pacienteAso));
    })
  }
  masPaginasDcha(): boolean {
    return this.pacienteAso?.infoPagina?.pageNumber != this.pacienteAso?.infoPagina?.totalPages;
  }

  masPaginasIzq(): boolean {
    return this.pacienteAso?.infoPagina?.pageNumber != 1;
  }

  sigPag(): void {
    if (this.masPaginasDcha()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum() + 1);
      this.recargarPag();
    }
  }
  antPag(): void {
    if (this.masPaginasIzq()) {
      this.pacienteService.setPageNum(this.pacienteService.getPageNum()- 1);
      this.recargarPag();
    }
  }*/
  buscardni() {
    this.pacienteService
    .getPaciente(this.datos.dni)
      .subscribe({
        next: (resp: Paciente[]) => {
        this.newPacienteAso=resp;
        this.pacienteAso=this.newPacienteAso;
        this.pacienteService.setPacs(this.pacienteAso);
        sessionStorage.setItem('pacientesAsociados', JSON.stringify(this.pacienteAso)); //guardo en memoria
        this.datos.dni="";
        document.getElementById('notFound').setAttribute('style','display: none;');
      }, 
      error: () => {
        this.pacienteAso=[];
        this.pacienteService.setPacs([]);
        this.datos.dni="";
        document.getElementById('notFound').setAttribute('style','display: contents;');
      },
    });
  }
  buscarnombre() {
    this.pacienteService
    .getPacienteNombre(this.datos.nombre)
      .subscribe({
        next: (resp: Paciente[]) => {
        this.pacienteAso=resp;
        this.pacienteService.setPacs(this.pacienteAso);
        sessionStorage.setItem('pacientesAsociados', JSON.stringify(this.pacienteAso)); //guardo en memoria
        this.datos.nombre="";
        document.getElementById('notFound').setAttribute('style','display: none;');
      },
      error: () => {
        this.pacienteAso=[];
        this.pacienteService.setPacs([]);
        this.datos.nombre="";
        document.getElementById('notFound').setAttribute('style','display: contents;');
      }
    });
  }
  buscarapellidos() {
    this.pacienteService
    .getPacienteApellidos(this.datos.apellidos)
      .subscribe({
        next: (resp: Paciente[]) => {
        this.pacienteAso=resp;
        this.pacienteService.setPacs(this.pacienteAso);
        sessionStorage.setItem('pacientesAsociados', JSON.stringify(this.pacienteAso)); //guardo en memoria
        this.datos.apellidos="";
        document.getElementById('notFound').setAttribute('style','display: none;');
      },
    error: () =>{
      this.pacienteAso=[];
        this.pacienteService.setPacs([]);
        this.datos.apellidos="";
        document.getElementById('notFound').setAttribute('style','display: contents;');
    }});
  }

  pacienteNuevo(){
    this.router.navigate(['/acceso/search/formulario']);
  }

  crearPrueba(dniPaciente: string){
    Swal.fire({
      title: '¿Estás seguro de empezar esta prueba?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Sí, adelante',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.value) {
         
        Swal.fire(
          'Hecho',
          'Prueba comenzando',
          'success'
        );
        this.prueba.dniPaciente=dniPaciente;
        this.prueba.dniDoctor=sessionStorage.getItem("userActual");
        if(sessionStorage.getItem("nuevaPrueba")=="pcr"){          
          this.pruebaService.crearPruebaPCR(this.prueba).subscribe();
        }
        if(sessionStorage.getItem("nuevaPrueba")=="sangre"){
          this.pruebaService.crearPruebaSangre(this.prueba).subscribe();
        }
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          'Cancelado',
          'La prueba no ha comenzado',
          'error'
        )
      }
    })
  }
  
  volver(){
    if(sessionStorage.getItem("nuevaPrueba")=="pcr"){          
      this.router.navigate(['/acceso/ctrlPanelPCR']);

    }
    if(sessionStorage.getItem("nuevaPrueba")=="sangre"){
      this.router.navigate(['/acceso/ctrlPanel']);
    }
  }
}
