import { HttpResponse } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observer, Subscription } from 'rxjs';
import { Prueba } from 'src/app/model/prueba/prueba';
import { PollingService } from 'src/app/service/polling.service';
import { PruebaService } from 'src/app/service/prueba.service';
import { PruebaSangreMetadata } from 'src/app/model/pruebaSangre/pruebaSangreMetadata';
import { PruebaSangreService } from 'src/app/service/pruebaSangre.service';
import { PruebaSangrePopUpComponent } from 'src/app/components/prueba-sangre-pop-up/prueba-sangre-pop-up.component'
import { MatDialog } from '@angular/material/dialog';


@Component({
  templateUrl: './control-panel.page.component.html',
  styleUrls: ['./control-panel.page.component.scss']
})
export class ControlPanelPageComponent implements OnInit {
  idAnalisis: number;
  idPCR: number;
  estado: boolean[] = [false, false]; //estado[0]: Analisis, estado[1]: PCR.
  tabla: string[] = ["Prueba_sangre", "Prueba_pcr"];
  error: string[] = ["errorA", "errorP"];
  input: string[] = ["inputDelA", "inputDelP"];
  subscription: Subscription;
  pruebasEjecucionSangre: Prueba[];
  pruebasEsperaSangre: Prueba[];
  pruebaSangre: PruebaSangreMetadata;

  constructor(private pruebaService: PruebaService, private router: Router, private pollingService: PollingService,
    private pruebaSangreService: PruebaSangreService, private popUpService: MatDialog) { }

  ngOnInit(): void {

    this.pollingService.pollingEjecucion('Prueba_sangre')
      .subscribe((respuesta: Prueba[]) => this.pruebasEjecucionSangre = respuesta);

    this.pollingService.pollingEspera('Prueba_sangre')
      .subscribe((respuesta: Prueba[]) => this.pruebasEsperaSangre = respuesta);

    sessionStorage.setItem("nuevaPrueba","sangre");
    this.pruebaSangreService.setPageNum(1);
    this.recargarPag();
  }

  segundos(segundos:number):number{
    return Math.round(segundos/1000);
  }

  popup(){
    document.getElementById('busqueda').setAttribute('open', 'open');
  }

  dialogPrueba(index : number, dialogo : string){
    if(!this.estado[index]){
      document.getElementById(dialogo).setAttribute('open', 'open');
      this.estado[index] = true;
    }else{
      document.getElementById(dialogo).removeAttribute('open');
      document.getElementById(this.error[index]).innerHTML = "";
      let HTMLInputElement, inputElement = <HTMLInputElement> document.getElementById(this.input[index]);
      inputElement.value = "";
      this.estado[index] = false;
    }
  }

  closeDialogPrueba(index : number, dialogo : string){
    this.estado[index]=false; 
    document.getElementById(dialogo).removeAttribute('open');
    document.getElementById(this.error[index]).innerHTML = "";
    let HTMLInputElement, inputElement = <HTMLInputElement> document.getElementById(this.input[index]);
    inputElement.value = "";
  }

  deletePrueba(type: number){
    
    this.pruebaService
    .deletePrueba(this.tabla[type], (type == 0? this.idAnalisis: this.idPCR))
    .subscribe({
      next: (response : HttpResponse<Prueba>) => {
        if(response.status == 200){           // Status Ok
          document.getElementById(this.error[type]).innerHTML = "Se ha eliminado correctamente";
          document.getElementById(this.error[type]).style.color = "black";
        }else if (response.status == 204){    // Status No Content (204)
          document.getElementById(this.error[type]).innerHTML = "No hay una prueba con ese id.";
          document.getElementById(this.error[type]).style.color = "red";
        }
      },
    });
    this.recargarPag();
  }

  volverInicio(){
    
  }

  logoff(){
    sessionStorage.removeItem("usuario");
    sessionStorage.removeItem("dni");
    sessionStorage.clear();

    this.router.navigate(['/']);
  }

  pruebasFinalizadasSangre(){
    if (this.pruebaSangreService.getPruSangre()!==null){
      this.pruebaSangre=this.pruebaSangreService.getPruSangre();
    }
    return this.pruebaSangre !== null;
  }


  sigPag(): void {
    if (this.masPaginasDcha()) {
      this.pruebaSangreService.setPageNum(this.pruebaSangreService.getPageNum() + 1);
      this.recargarPag();
    }
  }
  antPag(): void {
    if (this.masPaginasIzq()) {
      this.pruebaSangreService.setPageNum(this.pruebaSangreService.getPageNum()- 1);
      this.recargarPag();
    }
  }
  masPaginasDcha(): boolean {
    return this.pruebaSangre?.infoPagina?.pageNumber <this.pruebaSangre?.infoPagina?.totalPages;
  }

  masPaginasIzq(): boolean {
    return this.pruebaSangre?.infoPagina?.pageNumber > 1;
  }

  recargarPag(){
    this.pruebaSangreService.getPruebasPCR(this.pruebaSangreService.getPageNum(),this.pruebaSangreService.getPageSize())
    .subscribe((resp: PruebaSangreMetadata)=>{
      this.pruebaSangre=resp;
      this.pruebaSangreService.setPruSangre(resp);
      sessionStorage.setItem('pruebasSangre', JSON.stringify(resp));
    })
  }
  getFecha(fecha:number){
    const fechaRes = new Date(fecha);
    return(fechaRes.toLocaleDateString()+" "+fechaRes.toLocaleTimeString())
  }
  masInfo(electrolitos:number,glucosa:number,colesterol:number,trigliceridos:number,bilirrubina:number){
    const res = electrolitos+"/"+glucosa+"/"+colesterol+"/"+trigliceridos+"/"+bilirrubina;
    sessionStorage.setItem("datosSangre",res);
    this.popUpService.open(PruebaSangrePopUpComponent);
    this.recargarPag();
  }
  comprobar(numero:number){
    return numero!==null;
}
}
