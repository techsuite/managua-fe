import { HttpResponse } from '@angular/common/http';
import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observer, Subscription } from 'rxjs';
import { Prueba } from 'src/app/model/prueba/prueba';
import { PruebaPCRMetadata } from 'src/app/model/pruebaPcr/pruebaPCRMetadata';
import { PollingService } from 'src/app/service/polling.service';
import { PruebaService } from 'src/app/service/prueba.service';
import { PruebaPCRService} from 'src/app/service/pruebaPCR.service'
import { PruebaPcrPopUpComponent } from 'src/app/components/prueba-pcr-pop-up/prueba-pcr-pop-up.component'


@Component({
  templateUrl: './control-panel-pcr.page.component.html',
  styleUrls: ['./control-panel-pcr.page.component.scss']
})
export class ControlPanelPagePCRComponent implements OnInit {
  idAnalisis: number;
  idPCR: number;
  estado: boolean[] = [false, false]; //estado[0]: Analisis, estado[1]: PCR.
  tabla: string[] = ["Prueba_sangre", "Prueba_pcr"];
  error: string[] = ["errorA", "errorP"];
  input: string[] = ["inputDelA", "inputDelP"];
  subscription: Subscription;
  pruebasEjecucionPCR: Prueba[];
  pruebasEsperaPCR: Prueba[];
  pruebaPCR: PruebaPCRMetadata;

  constructor(private pruebaService: PruebaService, private router: Router, private pollingService: PollingService,
    private pruebaPCRService: PruebaPCRService, private popUpService: MatDialog) { }

  ngOnInit(): void {
    this.pruebaPCRService.setPageNum(1)
    this.recargarPag();

    this.pollingService.pollingEjecucion('Prueba_pcr')
      .subscribe((respuesta: Prueba[]) => this.pruebasEjecucionPCR = respuesta);

    this.pollingService.pollingEspera('Prueba_pcr')
      .subscribe((respuesta: Prueba[]) => this.pruebasEsperaPCR = respuesta);

    sessionStorage.setItem("nuevaPrueba","pcr");
  }

  segundos(segundos:number):number{
    return Math.round(segundos/1000);
  }

  popup(){
    document.getElementById('busqueda').setAttribute('open', 'open');
  }

  dialogPrueba(index : number, dialogo : string){
    if(!this.estado[index]){
      document.getElementById(dialogo).setAttribute('open', 'open');
      this.estado[index] = true;
    }else{
      document.getElementById(dialogo).removeAttribute('open');
      document.getElementById(this.error[index]).innerHTML = "";
      let HTMLInputElement, inputElement = <HTMLInputElement> document.getElementById(this.input[index]);
      inputElement.value = "";
      this.estado[index] = false;
    }
  }

  closeDialogPrueba(index : number, dialogo : string){
    this.estado[index]=false; 
    document.getElementById(dialogo).removeAttribute('open');
    document.getElementById(this.error[index]).innerHTML = "";
    let HTMLInputElement, inputElement = <HTMLInputElement> document.getElementById(this.input[index]);
    inputElement.value = "";
  }

  deletePrueba(type: number){
    
    this.pruebaService
    .deletePrueba(this.tabla[type], (type == 0? this.idAnalisis: this.idPCR))
    .subscribe({
      next: (response : HttpResponse<Prueba>) => {
        if(response.status == 200){           // Status Ok
          document.getElementById(this.error[type]).innerHTML = "Se ha eliminado correctamente";
          document.getElementById(this.error[type]).style.color = "black";
        }else if (response.status == 204){    // Status No Content (204)
          document.getElementById(this.error[type]).innerHTML = "No hay una prueba con ese id.";
          document.getElementById(this.error[type]).style.color = "red";
        }
      },
    });
    this.recargarPag();
  }

  pruebasFinalizadasPCR(){
    if (this.pruebaPCRService.getPruPCR()!==null){
      this.pruebaPCR=this.pruebaPCRService.getPruPCR();
    }
    return this.pruebaPCR !== null;
  }

  sigPag(): void {
    if (this.masPaginasDcha()) {
      this.pruebaPCRService.setPageNum(this.pruebaPCRService.getPageNum() + 1);
      this.recargarPag();
    }
  }
  antPag(): void {
    if (this.masPaginasIzq()) {
      this.pruebaPCRService.setPageNum(this.pruebaPCRService.getPageNum()- 1);
      this.recargarPag();
    }
  }
  masPaginasDcha(): boolean {
    return this.pruebaPCR?.infoPagina?.pageNumber < this.pruebaPCR?.infoPagina?.totalPages;
  }

  masPaginasIzq(): boolean {
    return this.pruebaPCR?.infoPagina?.pageNumber > 1;
  }

  recargarPag(){
    
    this.pruebaPCRService.getPruebasPCR(this.pruebaPCRService.getPageNum(),this.pruebaPCRService.getPageSize())
    .subscribe((resp: PruebaPCRMetadata)=>{
      this.pruebaPCR=resp;
      this.pruebaPCRService.setPruPCR(resp);
      sessionStorage.setItem('pruebasPCR', JSON.stringify(resp));
    })
  }

  logoff(){
    sessionStorage.removeItem("usuario");
    sessionStorage.removeItem("dni");
    sessionStorage.clear();

    this.router.navigate(['/']);
  }
  masInfo(flueorescencia:number){
    sessionStorage.setItem("fluorescencia",flueorescencia.toString());
    this.popUpService.open(PruebaPcrPopUpComponent);
    this.recargarPag();
  }

  getFecha(fecha:number){
 
      const fechaRes = new Date(fecha);
      return(fechaRes.toLocaleDateString()+" "+fechaRes.toLocaleTimeString())
    
  }
  
  comprobar(numero:number){
    return numero!==null;
  }
  
}
