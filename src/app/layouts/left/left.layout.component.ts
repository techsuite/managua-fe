import { Component } from '@angular/core';

@Component({
  selector: 'app-left-layout',
  template: `<ng-content></ng-content>`,
  styles: [
    `
      :host {
        display: flex;
        flex-direction: column;
        align-items: left;
        gap: 2rem;
        height: 100vh;
        padding: 5rem;
      }
    `,
  ],
})
export class LeftLayoutComponent {}