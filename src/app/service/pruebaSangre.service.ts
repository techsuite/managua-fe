import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PruebaSangreMetadata } from '../model/pruebaSangre/pruebaSangreMetadata';
import { ApiService } from './api.service';

@Injectable({
    providedIn:'root',
})

export class PruebaSangreService{
    pageNum:number=1;
    pruebasSangre: PruebaSangreMetadata = null;
    pageSize:number=10;

    constructor(private api: ApiService){}
    getPageSize():number{
        return this.pageSize;
    }
    getPageNum() {
        return this.pageNum;
    }
    
    setPageNum(num: number) {
        if(this.pruebasSangre!== null && num>this.pruebasSangre.infoPagina.totalPages){
            num=this.pruebasSangre.infoPagina.totalPages;
        }
        this.pageNum = num;
        sessionStorage.setItem('paginaActualSangre', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
    }

    getPruSangre(){
        return this.pruebasSangre;
    }
    setPruSangre(pru: PruebaSangreMetadata){
        this.pruebasSangre = pru;
    }

    getPruebasPCR(pageNumber:number, pageSize:number):Observable<PruebaSangreMetadata>{
        if(this.pruebasSangre!== null && pageNumber>this.pruebasSangre.infoPagina.totalPages){
            pageNumber=this.pruebasSangre.infoPagina.totalPages;
        }
        return this.api.get(`getPruebasFechaPaginacion/Prueba_sangre/Pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`);
    }
}