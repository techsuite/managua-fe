import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PruebaPCR } from '../model/pruebaPcr/pruebaPCR';
import { PruebaPCRMetadata } from '../model/pruebaPcr/pruebaPCRMetadata';
import { ApiService } from './api.service';

@Injectable({
    providedIn:'root',
})

export class PruebaPCRService{
    pageNum:number=1;
    pruebasPCR: PruebaPCRMetadata = null;
    pageSize:number=10;

    constructor(private api: ApiService){}
    getPageSize():number{
        return this.pageSize;
    }
    getPageNum() {
        return this.pageNum;
    }
    
    setPageNum(num: number) {
        if(this.pruebasPCR!== null && num>this.pruebasPCR.infoPagina.totalPages){
            num=this.pruebasPCR.infoPagina.totalPages;
        }
        this.pageNum = num;
        sessionStorage.setItem('paginaActualPCR', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
    }

    getPruPCR(){
        return this.pruebasPCR;
    }
    setPruPCR(pru: PruebaPCRMetadata){
        this.pruebasPCR = pru;
    }

    getPruebasPCR(pageNumber:number, pageSize:number):Observable<PruebaPCRMetadata>{
        if(this.pruebasPCR!== null && pageNumber>this.pruebasPCR.infoPagina.totalPages){
            pageNumber=this.pruebasPCR.infoPagina.totalPages;
        }
        return this.api.get(`getPruebasFechaPaginacion/Prueba_pcr/Pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`);
    }
}