import { HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Prueba } from '../model/prueba/prueba';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class PruebaService {
  constructor(private api: ApiService) {}

  getPruebasEjecucion(tipoPrueba: string): Observable<Prueba[]>{
    return this.api.get(`getPruebasEjecucion/${tipoPrueba}`);
  }

  getPruebasEspera(tipoPrueba: string): Observable<Prueba[]>{
    return this.api.get(`getPruebasEspera/${tipoPrueba}`);
  }
  // cogemos la prueba con el dni y la tabla a la que pertenece
  deletePrueba(tipoPrueba: string, id: number): Observable<HttpResponse<Prueba>> {
    return this.api.delete(`prueba/${tipoPrueba}/${id}`);
  }

  updateDatePrueba(tipoPrueba: string, id: number):  Observable<Prueba> {
    return this.api.patch(`updateDatePrueba/${tipoPrueba}/${id}`);
  }

  updatePrueba(tipoPrueba: string, id: number): Observable<Prueba> {
    return this.api.patch(`updatePrueba/${tipoPrueba}/${id}`);
  }
  crearPruebaPCR(prueba: Prueba):Observable<void>{
    return this.api.post(`addPruebaPCR`, prueba);
  }
  crearPruebaSangre(prueba: Prueba):Observable<void>{
    return this.api.post(`addPruebaSangre`, prueba);
  }
}