import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../model/usuario';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  constructor(private api: ApiService) {}
  // cogemos el usuario con el dni y la password
  getUsuario(dni: string, password: string): Observable<Usuario> {
    return this.api.get(`usuario/${dni}/${password}/`);
  }
}
