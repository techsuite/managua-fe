import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from '../model/paciente/paciente';
import { PacienteMetadata } from '../model/paciente/pacienteMedata';
import { ApiService } from './api.service';



@Injectable({
  providedIn: 'root',
})
export class PacienteService {
  constructor(private api: ApiService) {}
  // cogemos el paciente con el dni 
  pageNum: number=1;
  pacientes: Paciente[];
  pageSize: number=10;

  getPageSize():number{
    return this.pageSize;
  }
  getPageNum() {
    return this.pageNum;
  }

  setPageNum(num: number) {
    this.pageNum = num;
    sessionStorage.setItem('paginaActual', this.pageNum.toString()); //guardo en memoria la pagina en la que me encuentro para poder recargar y seguir avanzando bien
  }
  getPacs(){
    return this.pacientes;
  }
  setPacs(pacs: Paciente[]) {
    this.pacientes = pacs;
  }

  getPaciente(dni: string): Observable<Paciente[]> {
    return this.api.get(`paciente/${dni}`);
  }
  getPacienteNombre(nombre: string): Observable<Paciente[]> {
    return this.api.get(`nombre/${nombre}`);
  }
  getPacienteApellidos(apellidos: string): Observable<Paciente[]> {
    return this.api.get(`apellidos/${apellidos}`);
  }
  newPaciente(paciente: Paciente): Observable<Paciente> { // Sujeto a cambios
    return this.api.post('nuevoPaciente', paciente);
  }

  getPacientes(pageNumber:number, pageSize:number){
    return this.api.get(`getPacientesPaginacion/Pagina?pageNumber=${pageNumber}&pageSize=${pageSize}`)
  }
}
