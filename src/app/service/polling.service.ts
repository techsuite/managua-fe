import { Injectable } from "@angular/core";
import { interval, Observable } from "rxjs";
import { switchMap } from "rxjs/operators";
import { PruebaService } from "./prueba.service";

@Injectable({providedIn: 'root'})
export class PollingService{
    constructor(private pruebaService: PruebaService){}

    pollingEjecucion(tipoPrueba: string): Observable<any> {
        return interval(1000).pipe(
            switchMap((_) => {
                return this.pruebaService.getPruebasEjecucion(tipoPrueba);
            })
        );
    }

    pollingEspera(tipoPrueba: string): Observable<any> {
        return interval(1000).pipe(
            switchMap((_) => {
                return this.pruebaService.getPruebasEspera(tipoPrueba);
            })
        );
    }
}