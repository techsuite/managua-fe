import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root',
})
export class LoginService {

    private authId : string = "";

    constructor(private api: ApiService) {}

    isLoggedIn(): boolean {
        return this.authId != "";
    }

    setAuthId(authId: string) {
        this.authId = authId;
    }
}