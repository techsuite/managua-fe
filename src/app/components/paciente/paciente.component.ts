import { Component, Input, OnInit } from '@angular/core';
import { Paciente } from 'src/app/model/paciente/paciente';
import { PacienteService } from 'src/app/service/paciente.service';

@Component({
  selector: 'app-paciente',
  templateUrl: './paciente.component.html',
  styleUrls: ['./paciente.component.scss']
})
export class PacienteComponent implements OnInit {

  @Input() dni: string;

  usuario: Paciente;

  constructor(private pacienteService: PacienteService) { }

  ngOnInit(): void {
    // this.usuarioService.getUsuario(this.nombre).subscribe((usuario) => (this.usuario = usuario));
  }

}
