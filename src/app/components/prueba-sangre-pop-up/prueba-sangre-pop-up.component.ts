import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'prueba-sangre-pop-up',
    templateUrl: './prueba-sangre-pop-up.component.html',
    styleUrls: ['./prueba-sangre-pop-up.component.scss']
})

export class PruebaSangrePopUpComponent implements OnInit{
    datoElectrolitos: string;
    datoGlucosa: string;
    datoColesterol: string;
    datoTrigliceridos:string;
    datoBilirrubina:string;

    constructor(){}
    ngOnInit(): void{
        const res =sessionStorage.getItem("datosSangre").split("/");
        this.datoElectrolitos=res[0];
        this.datoGlucosa=res[1];
        this.datoColesterol=res[2];
        this.datoTrigliceridos=res[3];
        this.datoBilirrubina=res[4];
        if(this.datoElectrolitos=="0" && this.datoGlucosa=="0"){
            document.getElementById("datos").style.display="none";
            document.getElementById("nodatos").style.display="block";
        }
        else{
            document.getElementById("datos").style.display="block";
            document.getElementById("nodatos").style.display="none";
        }
    }
}