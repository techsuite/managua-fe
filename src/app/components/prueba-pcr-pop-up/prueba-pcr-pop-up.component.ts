import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'prueba-pcr-pop-up',
    templateUrl: './prueba-pcr-pop-up.component.html',
    styleUrls: ['./prueba-pcr-pop-up.component.scss']
})

export class PruebaPcrPopUpComponent implements OnInit{
    datoFluorescencia:string;

    constructor(){}
    ngOnInit(): void{
        this.datoFluorescencia=sessionStorage.getItem("fluorescencia")
    }
}