export class Usuario {
  nombreDoctor: string; // campo del usuario
  dniDoctor: string; // campo del DNI
  nombreUsuario: string; // nombre del usuario
  password: string; // contraseña
}
