
import { PruebaPCR } from "./pruebaPCR";

export class PruebaPCRMetadata{

    "pruebaPCR": PruebaPCR[];

    "infoPagina":{
        "pageSize":number,
        "pageNumber":number,
        "totalPages":number
    }
}