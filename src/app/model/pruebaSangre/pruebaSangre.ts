export class PruebaSangre {
  id: number;             // id prueba
  dniDoctor: string;
  dniPaciente: string;  
  fechaFinal: Date;
  electrolitos: number;
  glucosa: number;
  colesterol: number;
  trigliceridos:number;
  bilirrubina:number;

  constructor(){}


}