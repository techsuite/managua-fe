
import { PruebaSangre } from "./pruebaSangre";

export class PruebaSangreMetadata{
    "pruebaSangre": PruebaSangre[];

    "infoPagina":{
        "pageSize":number,
        "pageNumber":number,
        "totalPages":number
    }
}