export class Paciente {
  dniPaciente: string; // dni del paciente
  nombrePaciente: string; // nombre del paciente
  apellidosPaciente: string; // apellidos del paciente
  gender: string; // género
  birthday: string; // cumpleaños
  email: string; // email

  constructor(dni, nombre, apellidos, gender, cumple, email){
    this.dniPaciente = dni;
    this.nombrePaciente = nombre;
    this.apellidosPaciente = apellidos;
    this.gender = gender;
    this.birthday = cumple;
    this.email = email;
  }
}
