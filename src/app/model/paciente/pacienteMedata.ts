import { Paciente } from "./paciente";

export class PacienteMetadata{
    "pacientes": Paciente[];

    "infoPagina":{
        "pageSize":number,
        "pageNumber":number,
        "totalPages":number
    }
}