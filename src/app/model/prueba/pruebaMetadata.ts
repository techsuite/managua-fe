
import { Prueba } from "./prueba";

export class PruebaMetadata{
    "pruebas": Prueba[];

    "infoPagina":{
        "pageSize":number,
        "pageNumber":number,
        "totalPages":number
    }
}