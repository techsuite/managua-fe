import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ControlPanelPageComponent } from './pages/control-panel/control-panel.page.component';
import { ControlPanelPagePCRComponent } from './pages/control-panel-pcr/control-panel-pcr.page.component';
import { HomePageComponent } from './pages/home/home.page.component';
import { InfoPageComponent } from './pages/info/info.page.component';
import { WelcomePageComponent } from './pages/welcome/welcome.page.component';
import { SearchPageComponent } from './pages/search/search.page.component';
import { FormularioPageComponent } from './pages/formulario/formulario.page.component';
import { AuthGuardService } from './service/AuthGuardSevice'
import { AuthGuard } from 'guards/auth.guard';
import { AccesoComponent } from './pages/acceso/acceso.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'info', component: InfoPageComponent},
  { path: 'acceso', component: AccesoComponent, canActivate:[AuthGuard], children:[
    { path: 'welcome', component: WelcomePageComponent},
    { path: 'ctrlPanel', component: ControlPanelPageComponent},
    { path: 'ctrlPanelPCR', component: ControlPanelPagePCRComponent},
    { path: 'search', component: SearchPageComponent},
    { path: 'search/formulario', component: FormularioPageComponent},
  ]},
  { path: '**', redirectTo: 'welcome', pathMatch:'full'}
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuardService],
})

export class AppRoutingModule {}
